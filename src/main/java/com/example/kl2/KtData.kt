package com.example.kl2

open class KtData {
    var x1: String? = null
    var x2: String? = null
    override fun toString(): String {
        return "{x1:${x1}, x2:${x2}}"
    }
}