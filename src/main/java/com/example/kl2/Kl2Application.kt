package com.example.kl2

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
open class Kl2Application

fun main(args: Array<String>) {
    //测试和lombok的兼容性
    val testData = TestData("a", "b")
    testData.name = "test"
    println(testData.toKt())
    runApplication<Kl2Application>(*args)
}
