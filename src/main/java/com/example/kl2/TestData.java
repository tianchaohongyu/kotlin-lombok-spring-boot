package com.example.kl2;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TestData {

    private String name;

    private String sex;


    public KtData toKt() {
        KtData ktData = new KtData();
        ktData.setX1(getName());
        ktData.setX2(getSex());
        return ktData;
    }

    public static void main(String[] args) {
        System.out.println(1000);
    }
}
